# css-manga-reader

A manga-reader like page flipper only using pure CSS. 

It uses page hash to navigate between pages.

## Testing

Run it using ```php -S localhost:postnumber```

**Placeholder image by [picsum.photos](http://picsum.photos/)**