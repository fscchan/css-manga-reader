<html>
	<head>
		<title>A pure css manga reader</title>
		<meta name="description" content="A manga-reader like page flipper only using pure CSS. It uses page hash to navigate between pages." />
		<style>
			/* A pure CSS manga reader
			 * Author: Anssen "fsc" Augustus
			 * Twitter: @fscchan
			 * Description: It uses page hash to determine which page should be shown. 
			*/ 

			h1 {
				text-align: center;
			}
			
			h1 div {
				font-size: 0.6em;
			}

			.pages {
				height: 72vh;
				position: relative;
				text-align: center;
				vertical-align: middle;
			}
			
			.pages .control {
				position: absolute;
				width: 32px;
				text-align: center;
				vertical-align: middle;
				top: 0px;
				bottom: 0px;
				transition: background-color 0.3s ease-out;
			}
			
			.pages .control:hover {
				background-color: rgba(0, 0, 0, 0.6);
			}
			
			.pages .control.left {
				left: 0px;
			}
			
			.pages .control.right {
				right: 0px;
			}
			
			/* The page control */
			.pages, 
			.pages:first-of-type:not(:target) {
				display: none;
			}
			
			.pages:first-of-type, 
			.pages:target {
				display: block;
			}
		</style>
	</head>
	<body>
		<h1>
			A pure css manga reader
			<div>Click <a href="#page/0">here</a> to go to first page</div>
		</h1>
		<?php for($max = 18, $i = 0; $i < $max; $i++) { ?>
			<div id="page/<?php print($i); ?>" class="pages">
				<img src="https://picsum.photos/400/600/?image=<?php print(rand(1, 100)); ?>">
				<?php if($i > 0) { ?><a href="#page/<?php print($i - 1); ?>" class="control left"><</a><?php } ?>
				<?php if($i < $max - 1) { ?><a href="#page/<?php print($i + 1); ?>" class="control right">></a><?php } ?>
			</div>
		<?php } ?>
	</body>
</html>